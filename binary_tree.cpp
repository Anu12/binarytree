#include<iostream>
#include<queue>
using namespace std;

struct node {
	int data;
	struct node* left;
	struct node* right;
};

class BinaryTree {
public :
  //Giving memory to Node and setting up data value and pointers
  node* initializeNode(int d) {
  	 node* n = (node*)malloc(sizeof(node));
  	 n->data=d;
  	 n->left=NULL;
  	 n->right=NULL;
  	 return n;
  }

//+AB  -> root,left,right
  void preorderTraversal(node* root) {
     if(root == NULL) return;
     cout<<root->data<<" ";
     preorderTraversal(root->left);
     preorderTraversal(root->right);
  }

  //AB+  -> left,right, root
  void postorderTraversal(node* root) {

  }

  //A+B  -> left,root,right
  void inorderTraversal(node * root) {

  }

  //Breadth First Traversal
  void levelOrderTraversal(node* root) {
  	   if(!root) return;
       queue<node*> q;      //Queue for storing Node Pointers
       q.push(root);        //Queue contains it's first entry which is root

       while(!q.empty()) {
       	 node* temp = q.front();
       	 cout<<temp->data<<" ";        //Printing Values
       	 q.pop();

       	 if(temp->left)
       	 	 q.push(temp->left);

       	 if(temp->right)
       	    q.push(temp->right); 	
       }
  }


  //First Node of every level when viewed from left 
  void leftViewIterative(node* root) {
       int count_so_far = 0;
       if(!root) return;
       queue<node*> q;      
       q.push(root); 
       count_so_far  = 1;      
       bool flag = true;
       while(!q.empty()) {
         node* temp = q.front();
         if(flag) {
           cout<<temp->data<<" ";
           flag = false;
         }        
         q.pop();
         count_so_far--;

         if(temp->left)
             q.push(temp->left);

         if(temp->right)
            q.push(temp->right);  

         if(!count_so_far) {
            flag = true;
            count_so_far = q.size();
         }     
       }
  }

  //Last Node of every level when viewed from left side 
  //First Node of every level when viewed from right side 
  void rightViewIterative(node* root) {
       int count_so_far = 0;
       if(!root) return;
       queue<node*> q;      //Queue for storing Node Pointers
       q.push(root); 
       count_so_far  = 1;      
       bool flag = true;
       while(!q.empty()) {
         node* temp = q.front();
         if(flag) {
           cout<<temp->data<<" ";
           flag = false;
         }        
         q.pop();
         count_so_far--;

         if(temp->right)            //Processing Right Subtree first
            q.push(temp->right); 

         if(temp->left)
             q.push(temp->left);

         if(!count_so_far) {
            flag = true;
            count_so_far = q.size();
         }     
       }
  }

  //Making Counter and re-initializing it only when all nodes of previous level
  //has been processed
  void levelOrderTraversalLineByLine(node* root) {
       int count_so_far = 0;        //Counter
       if(!root) return;
       queue<node*> q;      
       q.push(root); 
       count_so_far  = 1;      
       bool flag = true;
       while(!q.empty()) {
         node* temp = q.front();
         cout<<temp->data<<" ";       
         q.pop();
         count_so_far--;          //Decrementing counter value side by side 

         if(temp->left)
             q.push(temp->left);

         if(temp->right)
            q.push(temp->right);  

         if(!count_so_far) {         //Finally re-initializing value of counter
            cout<<endl;
            count_so_far = q.size();
         }     
       }
  }

};


int main() {
    BinaryTree bt;
 
    node* root;     
    //bt.preorderTraversal(root);    Will give segementation fault , since no memory was allocated

    /*
    Created a Binary Tree 
         1
       /   \
      2     3
     /     /
    4     5
    
    */

    root = bt.initializeNode(1);
    root->left = bt.initializeNode(2);
    root->right = bt.initializeNode(3);
    root->left->left = bt.initializeNode(4);
    root->right->left = bt.initializeNode(5);

    //Calling preorder
    bt.preorderTraversal(root);
    cout<<endl<<endl;

    //Calling Level order
    bt.levelOrderTraversal(root);
    cout<<endl<<endl;
    bt.levelOrderTraversalLineByLine(root);
    cout<<endl<<endl;
    bt.leftViewIterative(root);
    cout<<endl;
    bt.rightViewIterative(root);

    return 0;
}

